# AngularN23

Proyecto de Angular con los ejercicios realizados en la clase 23 Noviembre de 2019.

En el proyecto veremos:

* Event handling a través de templates. [_value_] para property binding y (_event_) para event binding.

* **NgModel** y **FormsModule** para two way data binding (banana in a box).

* **Input** y **Output** para compartir datos entre componentes padres e hijos.

* Servicios y consumir APIs.

### Extra

El proyecto contendrá como bonus una arquitectura con Angular Routing para movernos por los ejemplos.